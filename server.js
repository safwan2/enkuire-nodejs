const express = require('express');
const mysql = require('mysql');
const bcrypt = require('bcrypt');
const app = express();
const port = 9000;


// Create a MySQL connection pool
const pool = mysql.createPool({
    host: 'localhost',
    user: 'root',
    password: 'local_password',
    database: 'enkuire',
    insecureAuth: true
});

// console.log(express);
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use((req, res, next) => {
    pool.getConnection((err, connection) => {
        if (err) {
            console.error('Error getting database connection:', err);
            return res.status(500).json({ error: 'Database error' });
        }

        // Attach the connection to the request object
        req.dbConnection = connection;

        // Continue to the next middleware or route handler
        next();
    });
});

// Define a route to handle API requests
app.post('/login', (req, res) => {
    const { email, password } = req.body;

    req.dbConnection.query("SELECT * FROM Users where email=?", [email], function(error, results, fields) {
        if (error) return res.status(500).json({ error: 'Error in query' });
        if (!results.length) return res.status(500).json({ error: 'Invalid credential' });
        const user = results[0];
        const hash = user.password.replace(/^\$2y(.+)$/i, '$2a$1');
        bcrypt.compare(password, hash, function(err, valid) {
            if (err || !valid) return res.status(500).json({ error: 'Invalid credential' });
            res.json({
                name: user.name,
                email: user.email,
                role: user.role,
                position: user.position,
            });
        });
    });
});
app.post('/send-otp', (req, res) => {
    const data = {
        success: true
    };
    res.json(req.body);
});
app.post('/verify-otp', (req, res) => {
    const data = {
        success: true
    };
    res.json(req.body);
});

app.post('/reset-password', (req, res) => {
    const data = {
        success: true
    };
    res.json(req.body);
});
// Start the server

app.listen(port, () => {
    console.log('Server is running on port ' + port);
});